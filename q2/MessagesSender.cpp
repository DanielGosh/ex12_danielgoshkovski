#include "MessagesSender.h"

std::vector<std::string> MessagesSender::_connected_users;

MessagesSender::MessagesSender()
{
	Signin();
}

void MessagesSender::Signin()
{
	bool check = false;
	std::string username;
	std::cout << "enter username: ";
	std::cin >> username;
	check = this->search(this->_connected_users, username);
	if (check == true)
	{
		std::cout << "bdihamin le shabat(already exists)";
	}
	else
	{
		this->_connected_users.push_back(username);
		this->_user_name = username;
		this->_exit_status = 0;
	}
}

void MessagesSender::Signout(std::string userName)
{
	this->_exit_status = 1;
}

void MessagesSender::ConnectedUsers()
{
	for (int i = 0;i<this->_connected_users.size();i++)
	{
		std::cout << this->_connected_users[i] << "\n";
	}
}

bool MessagesSender::search(std::vector<std::string> a, std::string b)
{
	for (int i = 0; i < a.size(); i++)
	{
		if (a[i] == b)
		{
			return true;
		}
	}
	return false;
}

void MessagesSender::readFromFile(std::string pth)
{
	int i = 0;
	std::ifstream infile(pth);
	std::string line;
	while (std::getline(infile, line))
	{
		this->_file_data.push_back(line);
		int position = line.find(line);
		if (position != std::string::npos) {
			line.replace(line.find(line), line.length(), "");
		}
		i++;
		// process pair (a,b)
	}
	for (int i = 0; i < this->_file_data.size(); i++)
	{
		std::cout << this->_file_data[i];
	}
	infile.close();
	std::ifstream f(pth, std::ofstream::out | std::ofstream::trunc);
	f.close();
}

void MessagesSender::broadCast(std::string pth)
{
	std::string msg;
	std::ofstream myfile;
	myfile.open(pth);
	for (int i = 0; i < this->_file_data.size(); i++)
	{
		myfile << this->_file_data[i];
		myfile << "\n";
	}
	myfile.close();
}


