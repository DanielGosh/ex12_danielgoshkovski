#pragma once
#include <iostream>
#include <vector>
#include <string>
#include <fstream>


class MessagesSender
{
private:
	std::vector<std::string> _file_data;
	int _exit_status; //1 for exit 0 for signed-in
	std::string _user_name;
public:
	static std::vector<std::string> _connected_users;
	MessagesSender();
	~MessagesSender();
	void Signin();
	void Signout(std::string userName);
	void ConnectedUsers();
	bool search(std::vector<std::string> a, std::string b); // helper function
	void readFromFile(std::string pth);
	void broadCast(std::string pth);
};

