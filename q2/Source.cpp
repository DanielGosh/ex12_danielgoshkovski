#include <iostream>
#include <vector>
#include <string>
#include <fstream>
#include "MessagesSender.h"
int main()
{
	std::string pthF;
	std::string pthT;
	MessagesSender* a = new MessagesSender();
	std::cout << "enter file to read from: ";
	std::cin >> pthF;
	a->readFromFile(pthF);
	std::cout << "enter file to write to: ";
	std::cin >> pthT;
	a->broadCast(pthT);
	return 0;
}
